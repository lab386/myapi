using ContosoManga.Models;
using ContosoManga.Services;
using Microsoft.AspNetCore.Mvc;

namespace ContosoManga.Controllers;

[ApiController]
// [Route("[controller]")]
[Route("api/Manga")]
public class MangaController : ControllerBase
{
    public MangaController()
    {
    }

    // GET all action
    [HttpGet]
    public ActionResult<List<Manga>> GetAll() =>
    MangaService.GetAll();

    // GET by Id action
    [HttpGet("{id}")]
    public ActionResult<Manga> Get(int id)
    {
       var Manga = MangaService.Get(id);

       if(Manga == null)
          return NotFound();

       return Manga;
    }

    
}