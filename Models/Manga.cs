namespace ContosoManga.Models;

public class Manga
{
    public int mal_id { get; set; }
    public string? title { get; set; }
    public bool publishing { get; set; }
    public string? synopsis { get; set; }
    public string? image_url { get; set; }
}
    
