using ContosoManga.Models;

namespace ContosoManga.Services;

public static class MangaService
{
    static List<Manga> Mangas { get; }
    static int nextId = 3;
    static MangaService()
    {
        Mangas = new List<Manga>
        {
            new Manga { mal_id = 9785, title = "Detective Conan: Conan vs. Kid - Shark & Jewel", publishing = true, synopsis= "3D IMAX movie, shown only at the Suntory Museum in Osaka.", image_url = "https://cdn.myanimelist.net/images/anime/13/27101.webp" },
            new Manga { mal_id = 9786, title = "Conan", publishing = true, synopsis= "test", image_url = "https://cdn.myanimelist.net/images/anime/13/27101.webp" }
        };
    }

    public static List<Manga> GetAll() => Mangas;

    public static Manga? Get(int id) => Mangas.FirstOrDefault(p => p.mal_id == id);

    public static void Add(Manga Manga)
    {
        Manga.mal_id = nextId++;
        Mangas.Add(Manga);
    }

    public static void Delete(int id)
    {
        var Manga = Get(id);
        if(Manga is null)
            return;

        Mangas.Remove(Manga);
    }

    public static void Update(Manga Manga)
    {
        var index = Mangas.FindIndex(p => p.mal_id == Manga.mal_id);
        if(index == -1)
            return;

        Mangas[index] = Manga;
    }
}